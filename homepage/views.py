from django.shortcuts import render

# Create your views here.

def templates(request):
    return render(request, 'templates.html')

def home(request):
    return render(request, 'home.html')

def mobilePayment(request):
    return render(request, 'mobile-payment.html')

def sandeza(request):
    return render(request, 'sandeza.html')

def messaging(request):
    return render(request, 'messaging.html')

def socialMessaging(request):
    return render(request, 'social-messaging.html')

def mobileAds(request):
    return render(request, 'mobile-ads.html')

def ussdPush(request):
    return render(request, 'ussd-push.html')

def emailMarkerting(request):
    return render(request, 'email-marketing.html')

def electronicVoucher(request):
    return render(request, 'electronic-voucher.html')

def about(request):
    return render(request, 'about.html')

def contact(request):
    return render(request, 'contact.html')

def all(request):
    return render(request, 'all.html')

def sprint360(request):
    return render(request, 'sprint360.html')