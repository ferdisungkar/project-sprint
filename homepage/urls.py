from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('templates', views.templates, name='templates'),
    path('sprint360', views.sprint360, name='sprint360'),
    path('mobile-payment', views.mobilePayment, name='mobile-payment'),
    path('sandeza', views.sandeza, name='sandeza'),
    path('messaging', views.messaging, name='messaging'),
    path('social-messaging', views.socialMessaging, name='social-messaging'),
    path('mobile-ads', views.mobileAds, name='mobile-ads'),
    path('ussd-push', views.ussdPush, name='ussd-push'),
    path('email-marketing', views.emailMarkerting, name='email-marketing'),
    path('electronic-voucher', views.electronicVoucher, name='electronic-voucher'),
    path('about', views.about, name='about'),
    path('contact', views.contact, name='contact'),
    path('all', views.all, name='all'),
]